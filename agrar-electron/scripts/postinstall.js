let fs = require('fs');

let sourcePath = '../@agrar/data';
let target = './node_modules/@agrar/data';

/*
if (fs.existsSync(target)) {
    console.log("Symlink " + target + " exists.")
    fs.unlinkSync(target);
    console.log("Symlink removed.")
} 

try {
    fs.unlinkSync(target);
} catch(error) {
    console.log("could not delete symlink" + target);
}
try {
    fs.unlinkSync(sourcePath);
} catch(error) {
    console.log("could not delete symlink" + sourcePath);
}
*/  
try {
    fs.symlinkSync(sourcePath, target, 'dir');
    console.log("created Symlink between " + sourcePath + " and " + target);
} catch(error) {
    console.log(error);
}
