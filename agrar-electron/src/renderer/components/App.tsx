import { Tier } from '@agrar/data';
import { Berechnung } from '@agrar/logik';
import { Button, MenuItem, Select } from '@material-ui/core';
import axios from "axios";
import * as React from 'react';

const rechner: Berechnung = new Berechnung();
const BACKEND: string = "http://localhost:3000"; 

interface IAppState {
  selectedTier: string,
  tiere: Array<Tier> 
}
class App extends React.Component<{}, IAppState> {

  constructor(props: any, state: IAppState) {
    super(props, state);
    this.state = {
      selectedTier: "",
      tiere: []
    }
  }

  handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({
      selectedTier: event.target.value
    }) 
  }

  public componentDidMount() {
    axios
      .get<Array<Tier>>(BACKEND + "/tiere")
      .then(response => {
        const tiere: Array<Tier> = [];
        console.log(response);
        response.data.map(tier => {
          tiere.push(tier);
        });
        this.setState({
          tiere: tiere
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

    public render() {
      let kuh: Tier = new Tier();
      kuh.name = "Karl";
      let esel: Tier = new Tier();
      esel.name = "Heinz";
      let erg: Tier = rechner.addiere(kuh, esel);
      let tiereSelect = (
        <Select value={this.state.selectedTier} displayEmpty={true} onChange={this.handleChange.bind(this)}>
          {
            this.state.tiere.map(tier => {
              return <MenuItem 
                key={tier.name} value={tier.name}>
                {tier.name}
                </MenuItem>
            })
          }
        </Select>
      ) 

      return (
        <div>
          <Button variant="raised" color="primary">Hallo {erg.name}</Button>
          {tiereSelect}
        </div>
      );
    }
  }
  
  export default App;