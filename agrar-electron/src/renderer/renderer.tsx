import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App';

// Import the styles here to process them with webpack
import './styles.scss';
import 'typeface-roboto';

ReactDOM.render(
    <App />,
    document.getElementById('app')
);