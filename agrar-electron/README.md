# agrar-electron
## Electron Desktop Anwendung
Quellcode liegt in den Ordnern
src/renderer: Quellcode für das React Frontend
src/main: Quellcode für den main Prozess des Electron Frameworks (backend)
### Wird benutzt von
-
### nutzt
@agrar/data: Geschäftsobjekte
@agrar/logik: Geschäftsregeln
React, Electron, TypeScript, WebPack
### Skripte
development: Kompiliert den Quellcode für den Development-Modus (Debugging)
production: Kompiliert den Quellcode produktionsreif
start: Startet die Electron Anwendung