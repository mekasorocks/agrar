# Innovativer Agrarrechner
## Projektstruktur

Agrarrechner

* agrar
* data
* logik
* services
* agrar-electron


### Skripte

**install-all** : Installiert alle benötigten Projekte und erstellt die Verbindungen

### manuelle Installation ###

**Data Projekt**

cd @ agrar/data

npm install

npm run compile

npm link

**Logik Projekt**

cd @ agrar/logik

npm install

npm link @ agrar/data

npm run compile

npm link

**Electron Projekt**

cd agrar-electron

npm install

npm link @ agrar/data

npm link @ agrar/logik

npm run development

npm start

### ToDo

Installation läuft noch nicht komplett automatisch

Leerzeichen zwischen @ und agrar ist nur für die readme wichtig (muss entfernt werden)

