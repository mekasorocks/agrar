import { Request, Response } from "express";
import {Tier} from "@agrar/data"

export class Routes {
  public routes(app): void {
    app.route("/").get((req: Request, res: Response) => {
      res.status(200).send({
        message: "backend steht zur Verfügung"
      });
    });

    app.route("/tiere").get((request: Request, response: Response) => {
      response.status(200).send(
        this.alleTiere()
      );
    });
  }

  private alleTiere = (): Array<Tier> => {
    const tiere: Array<Tier> = new Array<Tier>();
    let tier: Tier = new Tier();
    tier.name = "Ente";
    tiere.push(tier);
    tier = new Tier();
    tier.name = "Gans";
    tiere.push(tier);
    return tiere;
  };
}
