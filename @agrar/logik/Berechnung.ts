import {Tier} from '@agrar/data';
export class Berechnung {
    addiere = (tierA: Tier, tierB: Tier):Tier => {
        const kreuzung: Tier = new Tier();
        kreuzung.name = tierA.name + "-" + tierB.name;
        return kreuzung;
    }

    substrahiere(): number {
        return 0;
    }
}