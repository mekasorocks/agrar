# @agrar/logik
## Geschäftslogik (Services) für den innovativen Agrarrechner
Wurde angelegt mit gts (Google's TypeScript Style Guide).
npx gts init
npm install
### Wird benutzt von
agrar-electron : Die Desktop Anwendung
### nutzt
@agrar/data : Geschäftsobjekte
### Skripte
check: Lint & Check von Formatierungsregeln
fix: Korrigiert automatisch (wenn möglich) nach den Lint Regeln
clean: Löscht den Build-Ordner
compile": Kompiliert den TypeScript Code (nach ./build)